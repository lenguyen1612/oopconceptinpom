package com.myTest;

import org.testng.Assert;
import org.testng.annotations.Test;
import com.myPages.HomePage;
import com.myPages.LogInPage;
import com.myPages.RegisterPage;

public class LoginTest extends BaseTest {

	private RegisterPage res = null;
	String strUserID;
	String strPassword;

	@Test(priority = 1)
	public void verifyInfoRegistrationDisplayed() {
		res = page.getInstance(RegisterPage.class);
		res.registerUser();

		strUserID = res.getStrUsername();
		strPassword = res.getStrPassword();
		Assert.assertTrue(strUserID.length() > 0, "Verify User ID is displayed");
		Assert.assertTrue(strPassword.length() > 0, "Verify Password is displayed");
	}

	@Test(priority = 2)
	public void doLogin() {
		LogInPage loginPage = res.goToBankProject();
		HomePage homePage = loginPage.doLogin(strUserID, strPassword);
		
		String title = homePage.getTitleHomePage();
		Assert.assertEquals(title, "GTPL Bank Manager HomePage", "Verify Home Page opened");
	}
}
