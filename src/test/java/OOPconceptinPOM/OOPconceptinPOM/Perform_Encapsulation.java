package OOPconceptinPOM.OOPconceptinPOM;

public class Perform_Encapsulation {

	public static void main(String args[]) {
		//Tính đóng gói
		EncapsulationOOP encap = new EncapsulationOOP();
		encap.tmp = 7;		
		
		encap.setName("James");
		encap.setAge(20);
		encap.setIdNum("12343ms");

		System.out.print("Name : " + encap.getName() + " Age : " + encap.getAge());
	}
}
