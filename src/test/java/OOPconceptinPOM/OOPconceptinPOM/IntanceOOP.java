package OOPconceptinPOM.OOPconceptinPOM;

public class IntanceOOP {

	private String str = "";
	private int num = 0;
	
	public IntanceOOP() {
		
	}
	public IntanceOOP(String str) {
		this.str = str;
	}
	
	public IntanceOOP(String str, int num) {
		this.str = str;
		this.num = num;
	}
	
	
	public void strAndInt() {
		System.out.println("str: " + this.str + " int: " + this.num);
	}
	
	public void strOnly() {
		System.out.println("str: " + this.str);
	}
	
}
