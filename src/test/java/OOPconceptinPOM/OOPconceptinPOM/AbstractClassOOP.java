package OOPconceptinPOM.OOPconceptinPOM;

public abstract class AbstractClassOOP {

	  // Abstract method (does not have a body)
	  public abstract void animalSound();
	  // Regular method
	  public void sleep() {
	    System.out.println("Zzz");
	  }
}
